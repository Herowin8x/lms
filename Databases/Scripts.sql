USE [master]
GO
/****** Object:  Database [LMS]    Script Date: 5/22/2016 1:01:41 PM ******/
CREATE DATABASE [LMS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LMS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\LMS.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'LMS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\LMS.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [LMS] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LMS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LMS] SET ANSI_NULL_DEFAULT ON 
GO
ALTER DATABASE [LMS] SET ANSI_NULLS ON 
GO
ALTER DATABASE [LMS] SET ANSI_PADDING ON 
GO
ALTER DATABASE [LMS] SET ANSI_WARNINGS ON 
GO
ALTER DATABASE [LMS] SET ARITHABORT ON 
GO
ALTER DATABASE [LMS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LMS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LMS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LMS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LMS] SET CURSOR_DEFAULT  LOCAL 
GO
ALTER DATABASE [LMS] SET CONCAT_NULL_YIELDS_NULL ON 
GO
ALTER DATABASE [LMS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LMS] SET QUOTED_IDENTIFIER ON 
GO
ALTER DATABASE [LMS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LMS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LMS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LMS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LMS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LMS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LMS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LMS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LMS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LMS] SET RECOVERY FULL 
GO
ALTER DATABASE [LMS] SET  MULTI_USER 
GO
ALTER DATABASE [LMS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LMS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LMS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LMS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [LMS] SET DELAYED_DURABILITY = DISABLED 
GO
USE [LMS]
GO
/****** Object:  Table [dbo].[Holiday]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Holiday](
	[holidayID] [int] NOT NULL,
	[holidayName] [text] NOT NULL,
	[holidayDate] [date] NOT NULL,
	[actualDate] [date] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Leave_Allowance]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leave_Allowance](
	[leaveAllowanceID] [int] NOT NULL,
	[userID] [int] NOT NULL,
	[leaveTypeID] [int] NOT NULL,
	[leaveRemaining] [int] NOT NULL,
	[startedDate] [date] NULL,
	[expiredDate] [date] NULL,
 CONSTRAINT [PK_Leave_Allowance] PRIMARY KEY CLUSTERED 
(
	[leaveAllowanceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Leave_Request]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Leave_Request](
	[leaveRequestID] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NOT NULL,
	[leaveTypeID] [int] NOT NULL,
	[fromDate] [datetime] NOT NULL,
	[toDate] [datetime] NOT NULL,
	[isHaftDay] [bit] NULL,
	[isFirstHaft] [bit] NULL,
	[isSecondHaft] [bit] NULL,
	[attachment] [image] NULL,
	[requestStatus] [smallint] NOT NULL,
	[totalLeave] [int] NOT NULL,
	[leaveDescription] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[permissionID] [int] NOT NULL,
	[permissionName] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleID] [int] NOT NULL,
	[roleName] [text] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RoleAndPermission]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleAndPermission](
	[rolePermissionID] [int] NOT NULL,
	[roleID] [int] NOT NULL,
	[permissionID] [int] NOT NULL,
 CONSTRAINT [PK_RoleAndPermission] PRIMARY KEY CLUSTERED 
(
	[rolePermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Timming]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Timming](
	[timeID] [int] NOT NULL,
	[userID] [int] NOT NULL,
	[day] [datetime] NOT NULL,
	[timeIn] [datetime] NOT NULL,
	[timeOut] [datetime] NOT NULL,
	[manualIn] [datetime] NULL,
	[manualOut] [datetime] NULL,
	[comments] [text] NULL,
	[submit] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Type_Of_Leave]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Type_Of_Leave](
	[leaveTypeID] [int] NOT NULL,
	[name] [text] NOT NULL,
	[description] [text] NOT NULL,
	[allowanceLeave] [int] NULL,
	[isPaid] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 5/22/2016 1:01:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[userID] [int] NOT NULL,
	[holidayID] [int] NOT NULL,
	[firstName] [text] NOT NULL,
	[lastName] [text] NOT NULL,
	[dayOfBirth] [date] NOT NULL,
	[gender] [text] NOT NULL,
	[joinDate] [date] NOT NULL,
	[contactPhone] [nvarchar](11) NULL,
	[address] [text] NULL,
	[possition] [text] NOT NULL,
	[level] [nvarchar](3) NOT NULL,
	[emailAddr] [nvarchar](50) NOT NULL,
	[passWord] [nvarchar](16) NOT NULL,
	[roleId] [int] NULL,
	[approverId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON
GO
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (1, N'International New Year''s Day', CAST(N'2016-01-01' AS Date), CAST(N'2016-01-01' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (2, N'Tet holiday', CAST(N'2016-02-07' AS Date), CAST(N'2016-02-12' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (3, N'Tet holiday', CAST(N'2016-02-08' AS Date), CAST(N'2016-02-08' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (4, N'Tet holiday', CAST(N'2016-02-09' AS Date), CAST(N'2016-02-09' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (5, N'Tet holiday', CAST(N'2016-02-10' AS Date), CAST(N'2016-02-10' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (6, N'Tet holiday', CAST(N'2016-02-11' AS Date), CAST(N'2016-02-11' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (7, N'Vietnamese Kings Commemoration Day', CAST(N'2016-04-16' AS Date), CAST(N'2016-04-18' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (8, N'Liberation Day/Reunification Day', CAST(N'2016-04-30' AS Date), CAST(N'2016-05-02' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (9, N'International Labor Day', CAST(N'2016-05-01' AS Date), CAST(N'2016-05-03' AS Date))
INSERT [dbo].[Holiday] ([holidayID], [holidayName], [holidayDate], [actualDate]) VALUES (10, N'Independence Day', CAST(N'2016-09-02' AS Date), CAST(N'2016-09-05' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (1, 2, 1, 12, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (2, 2, 2, 3, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (3, 2, 3, 1, CAST(N'2016-01-06' AS Date), CAST(N'2016-07-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (4, 2, 4, 1, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (5, 2, 5, 30, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (6, 2, 6, 60, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (7, 2, 7, 3, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (8, 2, 8, 5, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (9, 2, 10, 720, CAST(N'2016-01-01' AS Date), NULL)
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (10, 2, 11, 30, CAST(N'2016-01-01' AS Date), CAST(N'2016-12-01' AS Date))
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (11, 4, 1, 12, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (12, 4, 2, 3, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (13, 4, 3, 1, N'2016-01-06', N'2016-07-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (14, 4, 4, 1, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (15, 4, 5, 30, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (16, 4, 6, 60, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (17, 4, 7, 3, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (18, 4, 9, 150, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (19, 4, 10, 720, N'2016-01-01', NULL)
INSERT [dbo].[Leave_Allowance] ([leaveAllowanceID], [userID], [leaveTypeID], [leaveRemaining], [startedDate], [expiredDate]) VALUES (20, 4, 11, 30, N'2016-01-01', N'2016-12-01')
INSERT [dbo].[Leave_Request] ([leaveRequestID], [userID], [leaveTypeID], [fromDate], [toDate], [isHaftDay], [isFirstHaft], [isSecondHaft], [attachment], [requestStatus], [leaveDescription]) VALUES (1, 2, 1, CAST(N'2016-05-05 00:00:00.000' AS DateTime), CAST(N'2016-05-06 00:00:00.000' AS DateTime), 0, 0, 0, NULL, 0, N'Des')
INSERT [dbo].[Leave_Request] ([leaveRequestID], [userID], [leaveTypeID], [fromDate], [toDate], [isHaftDay], [isFirstHaft], [isSecondHaft], [attachment], [requestStatus], [leaveDescription]) VALUES (2, 1, 6, CAST(0x0000A60C00000000 AS DateTime), CAST(0x0000A60F00000000 AS DateTime), NULL, NULL, NULL, NULL, 0, N'Go to Hospital')
SET IDENTITY_INSERT [dbo].[Leave_Request] ON
INSERT [dbo].[Permission] ([permissionID], [permissionName]) VALUES (1, N'RaiseRequest')
INSERT [dbo].[Permission] ([permissionID], [permissionName]) VALUES (2, N'ApproveRequest')
INSERT [dbo].[Permission] ([permissionID], [permissionName]) VALUES (3, N'ReceiveCCLeaveRequest')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (1, N'Employee')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (2, N'Manager')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (3, N'HR Leave Management')
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (1, 1, 1)
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (2, 2, 2)
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (3, 2, 1)
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (4, 4, 3)
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (5, 4, 1)
INSERT [dbo].[RoleAndPermission] ([rolePermissionID], [roleID], [permissionID]) VALUES (6, 4, 2)
INSERT [dbo].[Timming] ([timeID], [userID], [day], [timeIn], [timeOut], [manualIn], [manualOut], [comments], [submit]) VALUES (N'1', 1, CAST(N'2016-02-12 00:00:00.000' AS DateTime), CAST(N'2016-02-12 08:00:00.000' AS DateTime), CAST(N'2016-02-12 17:00:00.000' AS DateTime), NULL, NULL, NULL, NULL)
INSERT [dbo].[Timming] ([timeID], [userID], [day], [timeIn], [timeOut], [manualIn], [manualOut], [comments], [submit]) VALUES (N'2', 1, CAST(N'2016-02-13 00:00:00.000' AS DateTime), CAST(N'2016-02-13 09:00:00.000' AS DateTime), CAST(N'2016-02-13 17:00:00.000' AS DateTime), NULL, NULL, N'Take Child', NULL)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (1, N'Annual Leave (AL)', N'All eligible Associates are entitled to paid Annual Leave based on the below job grade for a full service year, otherwise AL entitlement will be calculated on pro-rata basis.', 15, 1)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (2, N'Bereavement Leave (BL)', N'Death of his/her parents (including his/her parents-in-law), his/her spouse or his/her legal child;Death of his/her grand-parents (paternal as well as maternal side),including grand-parents of his/her spouse, or death of any of his/her legal siblings;As for male Associate, wife''s delivery baby. ', 3, 1)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (3, N'Birthday Leave (BDL)', N'Leave to be taken on the actual birthday or within a month if business requires. No extension is allowed. The off day will be forfeited if no use within the time frame.', 1, 1)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (4, N'Child Marriage Leave (CML)', N'Marriage of his/her children', 1, 1)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (5, N'Child Care Leave (CC)', N'Any Associate shall be entitled to taking leave for caring her children in illness according to provisions of the Labor Code, provided that she must apply for such a leave and submit medical certification attested by competent doctors thereafter. Taking leave for caring his or her children in illness, the Associate concerned shall not be entitled to receiving his/her salary but shall declare his/her status to receive benefits under such provisions prescribed by insurance authority.', 30, 0)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (6, N'Sick Leave (SL)', N'Associates shall be entitled to sick leaves in accordance with their length of services in Company', 60, 0)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (7, N'Own Marriage Leave (OML)', N'His/her marriage', 3, 1)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (8, N'Parternity Leave (PL)', N'Father leave', 5, 0)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (9, N'Marernity Leave (ML)', N'Mother leave', 180, 0)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (10, N'Sabbatical leave (SBL)', N'The Sabbatical Leave is a special facility to the associates in order to enable them to update their knowledge and experience so that they will be of greater use to the organization on their rejoining.', 720, 0)
INSERT [dbo].[Type_Of_Leave] ([leaveTypeID], [name], [description], [allowanceLeave], [isPaid]) VALUES (11, N'Unpaid Leave (UL)', N'His/her family member, including his/her own and spousal parents and his/her legal spouse, gets sick and in need of personal care. Such other cases as deemed reasonable and approved by the relevant HODS.', 30, 0)
INSERT [dbo].[User] ([userID], [holidayID], [firstName], [lastName], [dayOfBirth], [gender], [joinDate], [contactPhone], [address], [possition], [level], [emailAddr], [passWord], [roleId], [approverId]) VALUES (1, 1, N'Tuan123', N'Nguyen Dang Hoang', CAST(N'1988-09-24' AS Date), N'Nam', CAST(N'2015-05-11' AS Date), N'0974498893', N'58 Ngo Thoi Nhiem, P17, Q.PN, HCM', N'Developer', N'3', N'hoangtuan24091988@gmail.com', N'123456', 1, 3)
INSERT [dbo].[User] ([userID], [holidayID], [firstName], [lastName], [dayOfBirth], [gender], [joinDate], [contactPhone], [address], [possition], [level], [emailAddr], [passWord], [roleId], [approverId]) VALUES (2, 1, N'An', N'Lam Truong', CAST(N'1992-02-05' AS Date), N'Nam', CAST(N'2013-06-21' AS Date), N'01234567891', N'Can Tho', N'Senior HR', N'4', N'1570733@hcmut.edu.vn', N'123456', 4, NULL)
INSERT [dbo].[User] ([userID], [holidayID], [firstName], [lastName], [dayOfBirth], [gender], [joinDate], [contactPhone], [address], [possition], [level], [emailAddr], [passWord], [roleId], [approverId]) VALUES (3, 1, N'Tien', N'Nguyen Van', CAST(N'1986-07-12' AS Date), N'Nam', CAST(N'2009-03-09' AS Date), N'0974123456', N'Go Vap', N'Manager', N'8', N'1570712@hcmut.edu.vn', N'123456', 2, NULL)
INSERT [dbo].[User] ([userID], [holidayID], [firstName], [lastName], [dayOfBirth], [gender], [joinDate], [contactPhone], [address], [possition], [level], [emailAddr], [passWord], [roleId], [approverId]) VALUES (4, 1, N'Thuy', N'Tran', CAST(N'1990-05-12' AS Date), N'Nữ', CAST(N'2009-03-09' AS Date), N'0974299443', N'Quan 1', N'Senior HR', N'5', N'tienspkt@gmail.com', N'123456', 4, NULL)
/****** Object:  Index [PK_HOLIDAY]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Holiday] ADD  CONSTRAINT [PK_HOLIDAY] PRIMARY KEY NONCLUSTERED 
(
	[holidayID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_LEAVE_REQUEST]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Leave_Request] ADD  CONSTRAINT [PK_LEAVE_REQUEST] PRIMARY KEY NONCLUSTERED 
(
	[leaveRequestID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_PERMISSION]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Permission] ADD  CONSTRAINT [PK_PERMISSION] PRIMARY KEY NONCLUSTERED 
(
	[permissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_ROLE]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [PK_ROLE] PRIMARY KEY NONCLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_ROLEANDPERMISSION]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[RoleAndPermission] ADD  CONSTRAINT [PK_ROLEANDPERMISSION] PRIMARY KEY NONCLUSTERED 
(
	[rolePermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PK_TIMMING]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Timming] ADD  CONSTRAINT [PK_TIMMING] PRIMARY KEY NONCLUSTERED 
(
	[timeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_TYPE_OF_LEAVE]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[Type_Of_Leave] ADD  CONSTRAINT [PK_TYPE_OF_LEAVE] PRIMARY KEY NONCLUSTERED 
(
	[leaveTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PK_USER]    Script Date: 5/22/2016 1:01:41 PM ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [PK_USER] PRIMARY KEY NONCLUSTERED 
(
	[userID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Leave_Allowance]  WITH CHECK ADD  CONSTRAINT [FK_LEAVE_AL_TYPE AND _TYPE_OF_] FOREIGN KEY([leaveTypeID])
REFERENCES [dbo].[Type_Of_Leave] ([leaveTypeID])
GO
ALTER TABLE [dbo].[Leave_Allowance] CHECK CONSTRAINT [FK_LEAVE_AL_TYPE AND _TYPE_OF_]
GO
ALTER TABLE [dbo].[Leave_Allowance]  WITH CHECK ADD  CONSTRAINT [FK_Leave_Allowance_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([userID])
GO
ALTER TABLE [dbo].[Leave_Allowance] CHECK CONSTRAINT [FK_Leave_Allowance_User]
GO
ALTER TABLE [dbo].[Leave_Request]  WITH CHECK ADD  CONSTRAINT [FK_LEAVE_RE_TYPE OF L_TYPE_OF_] FOREIGN KEY([leaveTypeID])
REFERENCES [dbo].[Type_Of_Leave] ([leaveTypeID])
GO
ALTER TABLE [dbo].[Leave_Request] CHECK CONSTRAINT [FK_LEAVE_RE_TYPE OF L_TYPE_OF_]
GO
ALTER TABLE [dbo].[Leave_Request]  WITH CHECK ADD  CONSTRAINT [FK_Leave_Request_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([userID])
GO
ALTER TABLE [dbo].[Leave_Request] CHECK CONSTRAINT [FK_Leave_Request_User]
GO
ALTER TABLE [dbo].[RoleAndPermission]  WITH CHECK ADD  CONSTRAINT [FK_ROLEANDP_HAVE IN R_PERMISSI] FOREIGN KEY([permissionID])
REFERENCES [dbo].[Permission] ([permissionID])
GO
ALTER TABLE [dbo].[RoleAndPermission] CHECK CONSTRAINT [FK_ROLEANDP_HAVE IN R_PERMISSI]
GO
ALTER TABLE [dbo].[RoleAndPermission]  WITH CHECK ADD  CONSTRAINT [FK_RoleAndPermission_Role] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[RoleAndPermission] CHECK CONSTRAINT [FK_RoleAndPermission_Role]
GO
ALTER TABLE [dbo].[Timming]  WITH CHECK ADD  CONSTRAINT [FK_Timming_User] FOREIGN KEY([userID])
REFERENCES [dbo].[User] ([userID])
GO
ALTER TABLE [dbo].[Timming] CHECK CONSTRAINT [FK_Timming_User]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Holiday] FOREIGN KEY([holidayID])
REFERENCES [dbo].[Holiday] ([holidayID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Holiday]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Role] FOREIGN KEY([roleId])
REFERENCES [dbo].[Role] ([roleID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Role]
GO
USE [master]
GO
ALTER DATABASE [LMS] SET  READ_WRITE 
GO
