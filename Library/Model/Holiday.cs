using System;
using System.Collections.Generic;

namespace Library.Model
{
	[Serializable]
	public partial class Holiday
	{
		public Holiday()
		{
			Users = new List<User>();		
		}
		public virtual DateTime ActualDate
		{
			get;
			set;
		}
		public virtual DateTime HolidayDate
		{
			get;
			set;
		}
		public virtual int HolidayId
		{
			get;
			set;
		}
		public virtual string HolidayName
		{
			get;
			set;
		}
		public virtual ICollection<User> Users
		{
			get;
			set;
		}
		
        //public override bool Equals(object obj)
        //{
        //    if (ReferenceEquals(this, obj))
        //        return true;
				
        //    return Equals(obj as Holiday);
        //}
		
        //public virtual bool Equals(Holiday obj)
        //{
        //    if (obj == null) return false;

        //    if (Equals(ActualDate, obj.ActualDate) == false) return false;
        //    if (Equals(HolidayDate, obj.HolidayDate) == false) return false;
        //    if (Equals(HolidayId, obj.HolidayId) == false) return false;
        //    if (Equals(HolidayName, obj.HolidayName) == false) return false;
        //    return true;
        //}
		
        //public override int GetHashCode()
        //{
        //    int result = 1;

        //    result = (result * 397) ^ ActualDate.GetHashCode();
        //    result = (result * 397) ^ HolidayDate.GetHashCode();
        //    result = (result * 397) ^ HolidayId.GetHashCode();
        //    result = (result * 397) ^ (HolidayName != null ? HolidayName.GetHashCode() : 0);
        //    return result;
        //}
	}
}