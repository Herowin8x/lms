using System;
using System.Collections.Generic;

namespace Library.Model
{
	[Serializable]
	public partial class TypeOfLeave
	{
		public TypeOfLeave()
		{
            //LeaveAllowances = new List<LeaveAllowance>();
            //LeaveRequests = new List<LeaveRequest>();		
		}
		public virtual int AllowanceLeave
		{
			get;
			set;
		}
		public virtual string Description
		{
			get;
			set;
		}
		public virtual bool? IsPaid
		{
			get;
			set;
		}
		public virtual int LeaveTypeId
		{
			get;
			set;
		}
		public virtual string Name
		{
			get;
			set;
		}
		public virtual ICollection<LeaveAllowance> LeaveAllowances
		{
			get;
			set;
		}
        public virtual ICollection<LeaveRequest> LeaveRequests
		{
			get;
			set;
		}
	}
}