using System;
using System.Collections.Generic;

namespace Library.Model
{
	[Serializable]
	public partial class LeaveAllowance
	{
		public LeaveAllowance()
		{		
		}
		public virtual DateTime? ExpiredDate
		{
			get;
			set;
		}
		public virtual int LeaveAllowanceId
		{
			get;
			set;
		}
		public virtual int LeaveRemaining
		{
			get;
			set;
		}
		public virtual DateTime? StartedDate
		{
			get;
			set;
		}
		public virtual TypeOfLeave TypeOfLeave
		{
			get;
			set;
		}
		public virtual User User
		{
			get;
			set;
		}
		
        //public override bool Equals(object obj)
        //{
        //    if (ReferenceEquals(this, obj))
        //        return true;
				
        //    return Equals(obj as LeaveAllowance);
        //}
		
        //public virtual bool Equals(LeaveAllowance obj)
        //{
        //    if (obj == null) return false;

        //    if (Equals(ExpiredDate, obj.ExpiredDate) == false) return false;
        //    if (Equals(LeaveAllowanceId, obj.LeaveAllowanceId) == false) return false;
        //    if (Equals(LeaveRemaining, obj.LeaveRemaining) == false) return false;
        //    if (Equals(StartedDate, obj.StartedDate) == false) return false;
        //    return true;
        //}
		
        //public override int GetHashCode()
        //{
        //    int result = 1;

        //    result = (result * 397) ^ (ExpiredDate != null ? ExpiredDate.GetHashCode() : 0);
        //    result = (result * 397) ^ LeaveAllowanceId.GetHashCode();
        //    result = (result * 397) ^ LeaveRemaining.GetHashCode();
        //    result = (result * 397) ^ (StartedDate != null ? StartedDate.GetHashCode() : 0);
        //    return result;
        //}
	}
}