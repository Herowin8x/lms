using System;
using System.Collections.Generic;

namespace Library.Model
{
    [Serializable]
    public partial class LeaveRequest
    {
        public LeaveRequest()
        {
            User = new User();
            TypeOfLeave = new TypeOfLeave();
        }

        public virtual byte[] Attachment
        {
            get;
            set;
        }
        public virtual DateTime FromDate
        {
            get;
            set;
        }
        public virtual bool? IsFirstHaft
        {
            get;
            set;
        }
        public virtual bool? IsHaftDay
        {
            get;
            set;
        }
        public virtual bool? IsSecondHaft
        {
            get;
            set;
        }
        public virtual string LeaveDescription
        {
            get;
            set;
        }
        public virtual int LeaveRequestId
        {
            get;
            set;
        }
        public virtual short RequestStatus
        {
            get;
            set;
        }
        public virtual DateTime ToDate
        {
            get;
            set;
        }

        public virtual int TotalLeave
        {
            get;
            set;
        }

        public virtual TypeOfLeave TypeOfLeave
        {
            get;
            set;
        }

        public virtual User User
        {
            get;
            set;
        }
    }
}