﻿using Library.Model;
using System;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface ILeaveRequestRepository
    {
        LeaveRequest Save(LeaveRequest model);
        LeaveRequest GetLeaveRequest(int requestID);
        IEnumerable<LeaveRequest> GetLeaveByUserId(int userId);
    }
}
