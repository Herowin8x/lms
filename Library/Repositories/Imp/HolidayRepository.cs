﻿using Library;
using Library.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Library.Repositories.Imp
{
    public class HolidayRepository : IHolidayRepository
    {
        public IEnumerable<Holiday> GetHolidays()
        {
            return UserRepository.session.QueryOver<Holiday>().List();
        }        
    }
}
