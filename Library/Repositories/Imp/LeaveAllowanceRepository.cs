﻿using Library;
using Library.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Library.Repositories.Imp
{
    public class LeaveAllowanceRepository : ILeaveAllowanceRepository
    {
        public IEnumerable<LeaveAllowance> GetLeaveAllowances(int userId)
        {
            return UserRepository.session
                .CreateCriteria(typeof(LeaveAllowance))
                .Add(Expression.Eq("User.UserId", userId))
                .List<LeaveAllowance>();
        }
    }
}
