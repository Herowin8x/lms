﻿using Library;
using Library.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Library.Repositories.Imp
{
    public class TypeOfLeaveRepository : ITypeOfLeaveRepository
    {
        public IEnumerable<TypeOfLeave> GetTypeOfLeaves()
        {
            return UserRepository.session.QueryOver<TypeOfLeave>().List();
        }        
    }
}
