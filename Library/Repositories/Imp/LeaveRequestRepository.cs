﻿using Library;
using Library.Model;
using NHibernate;
using NHibernate.Criterion;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Library.Repositories.Imp
{
    public class LeaveRequestRepository : ILeaveRequestRepository
    {
        public LeaveRequest Save(LeaveRequest model)
        {
            using (ITransaction transaction = UserRepository.session.BeginTransaction())
            {
                UserRepository.session.Save(model);
                transaction.Commit();
                return model;
            }            
        }

        public LeaveRequest GetLeaveRequest(int requestID)
        {
            LeaveRequest request = UserRepository.session.Get<LeaveRequest>(requestID);
            return request;
        }

        public IEnumerable<LeaveRequest> GetLeaveByUserId(int userId)
        {
            return UserRepository.session.CreateCriteria(typeof(LeaveRequest)).Add(Expression.Eq("User.UserId", userId)).List<LeaveRequest>();
        }
    }
}
