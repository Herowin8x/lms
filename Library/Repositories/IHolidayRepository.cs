﻿using Library.Model;
using System;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface IHolidayRepository
    {
        IEnumerable<Holiday> GetHolidays();
    }
}
