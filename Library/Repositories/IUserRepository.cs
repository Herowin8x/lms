﻿using Library.Model;
using System;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface IUserRepository
    {
        void Update(User user);
        User GetById(int userId);
        User GetByEmail(string email);
        IEnumerable<User> GetByName(string name); //firstname
    }
}
