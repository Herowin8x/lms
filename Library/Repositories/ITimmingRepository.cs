﻿using Library.Model;
using System;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface ITimmingRepository
    {
        IEnumerable<Timming> GetById(int userId);
    }
}
