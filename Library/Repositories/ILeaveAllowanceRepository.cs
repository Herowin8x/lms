﻿using Library.Model;
using System;
using System.Collections.Generic;

namespace Library.Repositories
{
    public interface ILeaveAllowanceRepository
    {
        IEnumerable<LeaveAllowance> GetLeaveAllowances(int userId);
    }
}
