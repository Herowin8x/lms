﻿using Library;
using Library.Model;
using Library.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web.Helper;

namespace Web.Controllers
{
    //TODO: will be double check late
    [CustomAuthFilter]
    [CustomExceptionFilter]
    public class TimmingsController : Controller
    {
        private readonly ITimmingRepository _repository;

        public TimmingsController(ITimmingRepository repository)
        {
            _repository = repository;
        }

        //TODO
        //[Route("Timming/Index123/{userId}")]
        public ActionResult Index(int userId)
        {
            return View(_repository.GetById(userId));
        }
    }
}
