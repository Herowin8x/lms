﻿using Library;
using Library.Model;
using Library.Repositories;
using System;
using System.Web;
using System.Web.Mvc;
using Web.Helper;

namespace Web.Controllers
{
    public class LeavesController : Controller
    {
        private readonly ILeaveRequestRepository _leaveRepository;
        private readonly ITypeOfLeaveRepository _typeOfLeaveRepository;
        private readonly IUserRepository _userRepository;
        private readonly IHolidayRepository _holidayRepository;

        public LeavesController(ILeaveRequestRepository leaveRepository, ITypeOfLeaveRepository typeOfLeaveRepository,
                                    IUserRepository userRepository, IHolidayRepository holidayRepository)
        {
            _leaveRepository = leaveRepository;
            _typeOfLeaveRepository = typeOfLeaveRepository;
            _userRepository = userRepository;
            _holidayRepository = holidayRepository;
        }

        public ActionResult Index()
        {
            //int userId = (int)Session["userId"];
            
            return View();
        }

        [HttpGet]
        public ActionResult Index(string name)
        {
            if (name != null && !String.IsNullOrEmpty(name))
            {
                var users = _userRepository.GetByName(name);
                ViewBag.ResultList = users;
            }
            return Index();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                int userId = int.Parse(collection["UserID"]);
                string action = "RaiseAndApproveRequest?userId=" + userId;
                string test = HttpUtility.UrlPathEncode(action);
                string test2 = HttpUtility.UrlDecode(action);

                return RedirectToAction("RaiseAndApproveRequest", new { userID = userId });
                //return RaiseAndApproveRequest(userId)
            }

            // Return to the user's homepage
            return RedirectToAction("Summary");
        }

        public ActionResult Policy()
        {
            return View();
        }

        public ActionResult RaiseAndApproveRequest(int userID)
        {
            ViewBag.TypeOfLeaves = _typeOfLeaveRepository.GetTypeOfLeaves();
            ViewBag.TargetEmployee = _userRepository.GetById(userID);
            ViewBag.Holidays = _holidayRepository.GetHolidays();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RaiseAndApproveRequest(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                var model = new LeaveRequest();
                model.User.UserId = int.Parse(collection["employeeId"]);
                model.TypeOfLeave.LeaveTypeId = int.Parse(collection["leaveType"]);
                model.FromDate = DateTime.Parse(collection["startDate"]);
                model.ToDate = DateTime.Parse(collection["endDate"]);
                model.LeaveDescription = collection["comments"];
                model.IsHaftDay = Common.CheckValidate(collection["isHaftDay"]);
                model.TotalLeave = int.Parse(collection["absentDay"]);
                model.RequestStatus = (int)LeaveRequestStatus.Approved;

                if (model.IsHaftDay.Value)
                {
                    model.IsFirstHaft = Common.CheckValidate(collection["isFirstHalf"]);
                    model.IsSecondHaft = !Common.CheckValidate(collection["isFirstHalf"]);
                }

                _leaveRepository.Save(model);

                var user = _userRepository.GetById(model.User.UserId);
                var manager = _userRepository.GetById(user.ApproverId);
                Common.SendEMail(user.EmailAddr, manager.EmailAddr, "hoangtuan24091988@gmail.com", model.LeaveRequestId);

                return RedirectToAction("Summary");
            }

            return View();
        }

        public ActionResult ApproveRequest(int requestID)
        {

            LeaveRequest request = _leaveRepository.GetLeaveRequest(requestID);
            ViewBag.LeaveRequest = request;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveRequest(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                int requestID = int.Parse(collection["LeaveRequestId"]);
                LeaveRequest request = _leaveRepository.GetLeaveRequest(requestID);

                string subject = "";
                string body = "";

                // Change leave request status
                bool approved = Boolean.Parse(collection["approve"]);
                if (approved)
                {
                    // Approved
                    request.RequestStatus = (int)LeaveRequestStatus.Approved;
                    subject = "Your leave request has been approved";
                    body = "Congratulation! The leave request #" + requestID + " has been approved.";
                }
                else {
                    // Rejected
                    request.RequestStatus = (int)LeaveRequestStatus.Rejected;
                    string reason = collection["approvercomment"];
                    subject = "Your leave request has been rejected";
                    body = "Your leave request (#" + requestID + ") has been rejected for a reason: " + reason;
                }
                _leaveRepository.Save(request);

                // Send mail to the employee and HR
                string employeeEmail = request.User.EmailAddr;

                string[] mails = { employeeEmail, "hoangtuan24091988@gmail.com" };
                Common.SendEMail(mails, subject, body);
            }

            // Return to the user's homepage
            return RedirectToAction("Summary");
        }

        public ActionResult NewRequest()
        {
            ViewBag.TypeOfLeaves = _typeOfLeaveRepository.GetTypeOfLeaves();
            ViewBag.Holidays = _holidayRepository.GetHolidays();
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewRequest(FormCollection collection)
        {
            if (ModelState.IsValid)
            {
                var model = new LeaveRequest();
                model.User.UserId = (int)Session["userId"];
                model.TypeOfLeave.LeaveTypeId = int.Parse(collection["leaveType"]);
                model.FromDate = DateTime.Parse(collection["startDate"]);
                model.ToDate = DateTime.Parse(collection["endDate"]);
                model.LeaveDescription = collection["comments"];
                model.IsHaftDay = Common.CheckValidate(collection["isHaftDay"]);
                model.TotalLeave = int.Parse(collection["absentDay"]);

                if (model.IsHaftDay.Value)
                {
                    model.IsFirstHaft = Common.CheckValidate(collection["isFirstHalf"]);
                    model.IsSecondHaft = !Common.CheckValidate(collection["isFirstHalf"]);
                }

                _leaveRepository.Save(model);

                var user = _userRepository.GetById(model.User.UserId);
                var manager = _userRepository.GetById(user.ApproverId);
                Common.SendEMail(user.EmailAddr, manager.EmailAddr, "hoangtuan24091988@gmail.com", model.LeaveRequestId);

                return RedirectToAction("Summary");
            }

            return View();
        }
                
        public ActionResult Summary()
        {
            int userId = (int)Session["userId"];
            var leaves = _leaveRepository.GetLeaveByUserId(userId);
            return View(leaves);
        }
    }
}