﻿using Library;
using Library.Model;
using Library.Repositories;
using Library.Repositories.Imp;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Web.Helper;

namespace Web.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserRepository _repository;
        private readonly ILeaveAllowanceRepository _leaveAllowanceRepository;

        public UsersController(IUserRepository repository,
                                    ILeaveAllowanceRepository leaveAllowanceRepository)
        {
            _repository = repository;
            _leaveAllowanceRepository = leaveAllowanceRepository;
        }

        public ActionResult Login()
        {
            //Check in a user will be logout
            if (UserRepository.isLogin == true)
            {
                SessionManager.CloseSession();
                UserRepository.isLogin = false;
            }

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User user)
        {
            User oldUser = _repository.GetByEmail(user.EmailAddr);
            if (oldUser != null && user.EmailAddr == oldUser.EmailAddr && user.PassWord == oldUser.PassWord)
            {
                UserRepository.isLogin = true;
                Session["fullName"] = oldUser.LastName + " " + oldUser.FirstName;
                Session["userId"] = oldUser.UserId;

                return RedirectToAction("Details/" + oldUser.UserId);
            }

            return View();
        }

        [CustomAuthFilter]
        [CustomExceptionFilter]
        public ActionResult Details(int id)
        {
            ViewBag.LeaveAllowance = _leaveAllowanceRepository.GetLeaveAllowances(id);
            return View(_repository.GetById(id));
        }

        public ActionResult Edit(int id)
        {
            return View(_repository.GetById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(User model)
        {
            if (ModelState.IsValid)
            {
                _repository.Update(model);
                Session["fullName"] = model.LastName + " " + model.FirstName;
                return RedirectToAction("Details/" + model.UserId);
            }

            return View(model);
        }
    }
}
