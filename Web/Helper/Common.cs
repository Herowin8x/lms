﻿using System.Net;
using System.Net.Mail;

namespace Web.Helper
{
    public enum LeaveRequestStatus{
        PendingForApproved = 0,
        Approved,
        Rejected,
        Cancelled
    }

    public class Common
    {
        public static void SendEMail(string[] mails, string subject, string body)
        {
            string smtpAddress = "smtp.mail.yahoo.com";
            int portNumber = 587;
            bool enableSSL = true;

            string emailFrom = "hoangtuan24091988@yahoo.com";
            string password = "duyanh12345";
            
            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);
                foreach(string m in mails){
                    mail.To.Add(new MailAddress(m));
                }
                
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Can set to false, if you are sending pure text.


                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        public static void SendEMail(string userEmail, string managerEmail, string hrEmail, int leaveRequestId)
        {
            string smtpAddress = "smtp.mail.yahoo.com";
            int portNumber = 587;
            bool enableSSL = true;

            string emailFrom = "hoangtuan24091988@yahoo.com";
            string password = "duyanh12345";
            string subject = "New Leave";
            string body = "Hello, This is a new leave request, Please take time help me approve who's as My Manager with a link as below\n http://localhost:60145/Leaves/ApproveRequest?requestID=" + leaveRequestId;

            using (MailMessage mail = new MailMessage())
            {
                mail.From = new MailAddress(emailFrom);

                mail.To.Add(new MailAddress(userEmail));
                mail.To.Add(new MailAddress(managerEmail));
                mail.To.Add(new MailAddress(hrEmail));

                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                // Can set to false, if you are sending pure text.

                //TODO
                //mail.Attachments.Add(new Attachment("D:\\VivaDatabase.bak"));

                using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                {
                    smtp.Credentials = new NetworkCredential(emailFrom, password);
                    smtp.EnableSsl = enableSSL;
                    smtp.Send(mail);
                }
            }
        }

        public static bool CheckValidate(string value)
        {
            return value == "on" ? true : false;
        }
    }
}
